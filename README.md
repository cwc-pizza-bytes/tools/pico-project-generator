# Pico Project Generator

Build it:
```bash
cargo build
```

Install it to your PATH:
```bash
cargo install --path .
```

Use it from anywhere:
```bash
# Requires internet access
pnew --help
```
