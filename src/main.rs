use anyhow::bail;
use anyhow::Result;
use clap::Parser;
use dialoguer::{theme::ColorfulTheme, Confirm};
use std::fs::File;
use std::io::prelude::*;
use url::Url;
use log::warn;
mod args;
mod downloaders;
use downloaders::{CachingFileGetter, FileGetter, FileGetterTrait};


fn create_output_dir(dir: &std::path::Path) -> Result<()> {
    if dir.exists() {
        let stop = Confirm::with_theme(&ColorfulTheme::default())
            .with_prompt(format!(
                "The output directory already {:?} exists, do you want to stop here?",
                dir
            ))
            .default(true)
            .interact()?;
        if stop {
            bail!("User cancelled operation as output directory already exists");
        }
        log::warn!("Removing {:?}", dir);
        std::fs::remove_dir_all(&dir)?;
    }

    std::fs::create_dir_all(&dir)?;
    Ok(())
}

fn create_cmakelists(dst: &std::path::Path, exe_name: &str, project_name: &str) -> Result<()> {
    let template = include_str!("res/CMakeLists.template");
    let complete = template.replace("<project_name>", project_name);
    let complete = complete.replace("<executable_name>", exe_name);
    let mut file = File::create(&dst)?;
    file.write_all(complete.as_bytes())?;
    Ok(())
}

#[doc = r"On github it looks like the pattern is projectname/projectname.c. 
There are also nested projects where the format is a/foo/foo.c"]
fn get_likely_source_filename_from_project_name(name: &str) -> &str {
    let token = "/";
    let parts: Vec<&str> = name.split(token).collect();
    parts.last().unwrap_or(&name)
}

fn create_template_url(source: Url, project_name: &str, filename: &str) -> Result<Url>{
    Url::parse(&format!("{}{}/{}.c", source, project_name, filename)).map_err(|parse_error| anyhow::Error::new(parse_error))
}


fn main() -> Result<()> {
    env_logger::init();

    let args = args::CLI::parse();

    let project_name = args.project_name.unwrap_or(format!("{}_project", args.executable_name));
    let project_sdk_file = &args.output_dir.join("pico_sdk_import.cmake");
    let project_cmakelists_file = &args.output_dir.join("CMakeLists.txt");

    if args.reset_cache {
        std::fs::remove_dir_all(&args.cache_path)?;
    }

    let downloader: Box<dyn FileGetterTrait> = match args.dont_cache {
        true => Box::new(FileGetter::new()),
        false => Box::new(CachingFileGetter::new(args.cache_path)),
    };

    create_output_dir(&args.output_dir)?;
    downloader.download(args.download_url, project_sdk_file)?;

    create_cmakelists(
        project_cmakelists_file,
        &args.executable_name,
        &project_name,
    )?;

    match args.template_project_name{
        Some(name) => {
            let n = get_likely_source_filename_from_project_name(&name);
            let url = create_template_url(args.template_project_source, &name, n)?;
            let project_main = args.output_dir.join(format!("{}.c", n));
            match downloader.download(url, &project_main) {
                Ok(_) => (), 
                Err(msg) => {
                    warn!("{}", msg);
                    std::process::exit(exitcode::UNAVAILABLE);
                }
            }
        },
        None => (),
    };
    
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::str::FromStr;

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    fn get_file_contents<P: AsRef<std::path::Path>>(file: P) -> Result<String> {
        Ok(std::fs::read_to_string(file)?)
    }


    #[test]
    fn test_create_template_name(){
        let source = Url::from_str("http://www.example.com").unwrap();
        let project_name = "b";
        let filename = "c";
        let actual = create_template_url(source, project_name, filename).unwrap();
        assert_eq!(url::Url::from_str("http://www.example.com/b/c.c").unwrap(), actual);
    }

    #[test]
    fn test_get_likely_filename_when_nested(){
        let actual = get_likely_source_filename_from_project_name("hello/there");
        assert_eq!("there", actual); 
    }

    #[test]
    fn test_get_likely_filename_when_simple(){
        let actual = get_likely_source_filename_from_project_name("blink");
        assert_eq!("blink", actual); 
    }    

    #[test]
    fn test_create_cmakelists() {
        let dst = std::env::temp_dir().join("test_cmake_lists");
        create_cmakelists(&dst, "testexe", "testproject").unwrap();
        let actual = get_file_contents(dst).unwrap();
        assert_eq!(
            "cmake_minimum_required(VERSION 3.13) 
include(pico_sdk_import.cmake)
project(testproject C CXX ASM) 
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17) 
pico_sdk_init()
add_executable(testexe 
    main.c
)
pico_enable_stdio_usb(testexe 1)
pico_enable_stdio_uart(testexe 1) 
pico_add_extra_outputs(testexe)
target_link_libraries(testexe pico_stdlib)",
            actual
        );
    }
}
