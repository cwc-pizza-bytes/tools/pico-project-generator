use anyhow::bail;
use anyhow::Result;
use log::debug;
use reqwest;
use std::io;
use url::Url;

fn download(url: Url) -> Result<String> {
    let error_msg = format!("Unable to get {}", url);
    debug!("Getting {:?}", url);
    let response = reqwest::blocking::get(url)?;
    match response.status() {
        reqwest::StatusCode::OK => Ok(response.text()?),
        _ => bail!(format!("{}: {}", error_msg, response.status())),
    }
}
pub trait FileGetterTrait {
    fn download(&self, source: Url, destination: &std::path::Path) -> Result<()>;
}

pub struct CachingFileGetter<P: AsRef<std::path::Path> + std::fmt::Debug> {
    cache_path: P,
}

pub struct FileGetter {}

impl<P: AsRef<std::path::Path> + std::fmt::Debug> CachingFileGetter<P> {
    pub fn new(cache_path: P) -> Self {
        CachingFileGetter { cache_path }
    }
    fn cache_filename<P2: AsRef<std::path::Path> + std::fmt::Debug>(
        &self,
        destination: P2,
    ) -> Result<std::path::PathBuf> {
        match destination.as_ref().file_name() {
            Some(name) => Ok(self.cache_path.as_ref().join(name)),
            None => bail!("No filename found in {:?}", destination.as_ref()),
        }
    }

    fn put_into_file<P2: AsRef<std::path::Path> + std::fmt::Debug>(
        &self,
        dst: P2,
        contents: &String,
    ) -> Result<()> {
        debug!("Copying {} characters into file {:?}", contents.len(), dst);
        let mut out = std::fs::File::create(dst)?;
        io::copy(&mut contents.as_bytes(), &mut out)?;
        Ok(())
    }
}

impl FileGetter {
    pub fn new() -> Self {
        FileGetter {}
    }
}

impl<P: AsRef<std::path::Path> + std::fmt::Debug> FileGetterTrait for CachingFileGetter<P> {
    fn download(&self, source: Url, destination: &std::path::Path) -> Result<()> {
        let cache_fname = self.cache_filename(&destination)?;
        debug!(
            "Looking for {:?} in the cache {:?}",
            cache_fname, self.cache_path
        );
        if cache_fname.exists() {
            debug!("{:?} is in the cache {:?}", cache_fname, self.cache_path);
            std::fs::copy(cache_fname, &destination)?;
            return Ok(());
        }

        debug!(
            "{:?} is not in the cache {:?}",
            cache_fname, self.cache_path
        );

        if !self.cache_path.as_ref().exists() {
            debug!(
                "Creating a directory to hold the cache in {:?}",
                self.cache_path
            );
            std::fs::create_dir_all(&self.cache_path)?;
        }

        let contents = download(source)?;

        self.put_into_file(cache_fname, &contents)?;
        self.put_into_file(destination, &contents)?;
        Ok(())
    }
}
impl FileGetterTrait for FileGetter {
    fn download(&self, source: Url, destination: &std::path::Path) -> Result<()> {
        let contents = download(source)?;
        let mut out = std::fs::File::create(destination)?;
        io::copy(&mut contents.as_bytes(), &mut out)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::FileGetterTrait;
    use super::*;

    fn hw_url() -> Url {
        Url::parse("https://gist.githubusercontent.com/nickyonge/e3864f0dd12eefc90c0e36e8dbd58796/raw/3b344e03590c322deaba91b45386051f09ed706c/helloworld.txt").unwrap()
    }

    fn get_file_contents<P: AsRef<std::path::Path>>(file: P) -> Result<String> {
        Ok(std::fs::read_to_string(file)?)
    }

    #[test]
    fn test_file_getter() {
        let uut = FileGetter::new();
        let dst = std::env::temp_dir().join("test_file_getter");
        uut.download(hw_url(), &dst).unwrap();
        assert!(dst.exists());
        assert_eq!("Hello, world!", get_file_contents(dst).unwrap());
    }

}
