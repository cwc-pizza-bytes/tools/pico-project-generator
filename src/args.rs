use clap::Parser;
use url::Url;


#[derive(Parser, Debug)]
#[command(
    version,
    about = "A tool to create a new Pico project. This requires an internet connection to go and grab required files, else they need to be present in the cache"
)]
pub struct CLI {
    #[arg(short, long, help = "The name of your project")]
    pub project_name: Option<String>,

    #[arg(help = "The name of the executuable within your project")]
    pub executable_name: String,

    #[arg(short,long, help="The location of the cache on your computer", value_hint=clap::ValueHint::DirPath, default_value_t=std::env::temp_dir().join(std::env::current_exe().unwrap().as_path().file_name().unwrap()).into_os_string().into_string().unwrap())]
    pub cache_path: String,

    #[arg(help="Where to put the created project", value_hint=clap::ValueHint::DirPath)]
    pub output_dir: std::path::PathBuf,

    #[arg(long, help="The URL of the pico_sdk_import file which is required", default_value_t=Url::parse("https://raw.githubusercontent.com/raspberrypi/pico-sdk/master/external/pico_sdk_import.cmake").unwrap())]
    pub download_url: Url,

    #[arg(
        short,
        long,
        help = "If you want to get an example proect this is the name of it"
    )]
    pub template_project_name: Option<String>,

    #[arg(long, help="If you want to get a template project, this is the URL where it can be found",default_value_t=Url::parse("https://raw.githubusercontent.com/raspberrypi/pico-examples/master/").unwrap())]
    pub template_project_source: Url,

    #[arg(long, short, help = "Reset the cache", default_value_t = false)]
    pub reset_cache: bool,

    #[arg(long, short, help = "Dont use the cache", default_value_t = false)]
    pub dont_cache: bool,
}
